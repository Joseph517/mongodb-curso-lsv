from bson.objectid import ObjectId  # noqa

# from car.databases.client import MongoLsv
from client import MongoLsv

mongo_lsv = MongoLsv()

print(
    mongo_lsv.get_records_from_collection(
        db_name="car_lsv", collection="monument"
    )
)

print(
    mongo_lsv.delete_record_in_collection(
        db_name="car_lsv",
        collection="monument",
        record_id="62b13b7c090c33dd3fa18440",
    )
)

print(
    mongo_lsv.get_records_from_collection(
        db_name="car_lsv", collection="monument", limit_value=20)
)
