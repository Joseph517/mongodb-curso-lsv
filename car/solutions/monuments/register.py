from bson.objectid import ObjectId  # noqa

# from car.databases.client import MongoLsv
from client import MongoLsv


mongo_lsv = MongoLsv()
print(
    mongo_lsv.get_records_from_collection(
        db_name="car_lsv", collection="monument"
    )
)

print(
    mongo_lsv.create_new_record_in_collection(
        db_name="car_lsv",
        collection="monument",
        record={"monument_name": "Monumento Washington",
                "description": "Emblemático obelisco de referencia en el National Mall que rinde honor al primer presidente estadounidense.",
                "country": "62b138f7a662e9be49d5be88"},
    )
)

print(
    mongo_lsv.get_records_from_collection(
        db_name="car_lsv", collection="monument", limit_value=20)
)
