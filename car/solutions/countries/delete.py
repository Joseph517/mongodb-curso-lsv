from bson.objectid import ObjectId  # noqa

# from car.databases.client import MongoLsv
from client import MongoLsv

mongo_lsv = MongoLsv()
print(mongo_lsv.list_dbs())
print(mongo_lsv.list_collections(db_name="car_lsv"))

print(
    mongo_lsv.get_records_from_collection(
        db_name="car_lsv", collection="countries"
    )
)

print(
    mongo_lsv.delete_record_in_collection(
        db_name="car_lsv",
        collection="countries",
        record_id="62b138f7a662e9be49d5be88",
    )
)

print(
    mongo_lsv.get_records_from_collection(
        db_name="car_lsv", collection="countries", limit_value=20)
)
