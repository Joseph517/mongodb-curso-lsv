from dataclasses import asdict, dataclass
from datetime import datetime

from pytz import country_names


@dataclass
class Country:
    uuid: str
    country: str
    capital: str

    def to_dict(self) -> dict:
        return asdict(self)


@dataclass
class monument:
    uuid: str  # UUID
    monument_name: str
    description: str
    country: Country  # puede llevar uuid o id de document_type

    def to_dict(self) -> dict:
        return asdict(self)
